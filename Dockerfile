FROM golang
WORKDIR /opt/hugo
ENV HUGO_VERSION 0.49
ENV HUGO_BIN_NAME hugo_${HUGO_VERSION}_Linux-64bit.tar.gz
ENV HUGO_BIN_URL https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/${HUGO_BIN_NAME}
RUN wget -qO- "${HUGO_BIN_URL}" | tar xz
ENV PATH "/opt/hugo:${PATH}"
COPY . .
RUN hugo

FROM nginx
COPY --from=0 /opt/hugo/public /usr/share/nginx/html