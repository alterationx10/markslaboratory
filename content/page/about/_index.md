---
title: "About"
date: 2018-10-06T17:16:00-04:00
subtitle: "Where are we going, and what's that hand basket for?"
tags: []
---

![Scary picture of Mark](mark.jpg)

Oh! Hello there. Welcome to my site!

What's that? _Scary picture_? Oh, yeah, I guess it is a bit menacing. It
was actually generated using __neural style transfer__ from the images below

{{< columns >}}
![Less scary picture of Mark](less-scary.jpg)
A slighlty less scary picture of me
{{< column >}}
![Der Tod als Erwurger by Alfred Rethel](erwurger.jpeg)
Der Tod als Erwurger by Alfred Rethel: A wood carving from the 1850's
{{< endcolumns >}}

[Neural style transfer](https://arxiv.org/abs/1508.06576) (re)composes
an image from the _style_ of a reference image using neural networks. You of course can't do
one of these without the obligatory 'Starry Night'

![Starry Mark](starry-mark.jpg)


_Where were we? Oh, yes..._

Well then, welcome to my personal blog! This site is where I plan to
post up my _mad science_ misadventures about how to do things as above,
for example, as well as other project write-ups.

For a less-nonsensical bio:

I am a currently a Senior Scala Engineer at Blue Orange Digital.

I have experience building and automating platforms that provide
advanced analytics and interfaces to customers, as well as enhanced
operational tools for internal service provisioning using awesome tools
like __Scala__, __Akka__, __Play Framework__, and __Kuberentes__.

I was also part of a small start-up team focusing on
bringing products to market from the ground up. My primary focus was
working with firmware engineers to write libraries and customer facing
applications (iOS, Android, PC) that could talk to our sensor devices
via Bluetooth.

I received my PhD before that, where my studies focused on QM calculations relating to chiroptical properties of transition
metal complexes. For a list of my publications, check out my 
[Google Scholar Page](https://scholar.google.com/citations?user=Xu1j0IMAAAAJ&hl=en)

### Senior Scala Engineer @ Blue Orange Digital
[2018 - Present]

### Full Stack Developer @ Vaspian LLC
[2014 - 2018]

### Scientist / Software Developer @ Sensorcon, Inc.
[2011 - 2014]
  
### PhD Theoretical Chemistry @ University at Buffalo
[2008 - 2012]
