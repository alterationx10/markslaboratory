---
title: A quick 3DF Zephyr scan
date: 2018-12-20
tags: 
  - 3df
  - 3d
  - photogrammetry
---

With the release of 3DF Zephyr 4.3, they added an option to export a 'self hosted' model that uses three.js!
I ended up tweaking my three.js code from one of their [examples](https://threejs.org/examples/), 
along with a Hugo shortcode to drop in the files and quickly add it to the site! Check
out the site [source code](https://gitlab.com/alterationx10/markslaboratory) if you want to have a look.

Here is a quick build/export of the first project I scanned in with the program.

Expect to see some more 3D models scanned in soon!

<!--more-->

{{< 3dfz obj="sample.obj" mtl="sample.mtl" name="Mask">}}
